import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operator/map';

@Injectable()
export class TaskService {

  constructor(private http:Http) {
    console.log("Task Service Initalized");
  }

  getTasks(){
    return this.http.get("http://localhost:3000/api/tasks").map(res=>res.json());
  }

  addTask(newTask){
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/task',JSON.stringify(newTask),{headers: headers}).map(res=>res.json());
  }

  deleteTask(taskID){
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.delete("http://localhost:3000/api/task/" + taskID);
  }

  updateTasks(task){
    console.log("Got to put")
    console.log(task);
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.put('http://localhost:3000/api/task/' + task._id,JSON.stringify(task),{headers: headers}).map(res=>res.json());
  }

}
