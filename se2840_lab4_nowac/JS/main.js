var symbols = []; //List of symbols being displayed
var imageNames = [ //List of symbols that have images associated with them
    "GOOGL",
    "AMD",
    "MSFT",
    "FB",
    "AMZN"
];

var hidden = true; //toggle for trashcan

$(document).ready(function() {
    var dataDump = $('#dataDump');
    //Headers for the table     
    var headers = [
        "Symbol",
        "Logo",
        "Price",
        "Change",
        "% Change",
        "Low",
        "High",
        "Volume",
        "Name",
        "Market Cap",
        "Exchange",
        ""
    ];
    //Generate the table
    var table = $("<table/>");
    table.addClass('table');
    table.addClass("table-striped")
    //Generate the headers
    var row = $(table[0].insertRow(-1));
    for(var i = 0; i < headers.length; i++){
        var headerCell = $("<th/>");
        headerCell.html(headers[i]);
        row.append(headerCell);
    }
    dataDump.append(table);
    
    //Get stock data with JSON request
    $('#getData').click(function(){
        $("#status").text("Loading...");
        //Get the json data
        $.getJSON('https://stock-quoter-se2840.herokuapp.com/stock/'+$("#symbol").val().toUpperCase()+'/dump',function(data){
            try{
                d = jQuery.parseJSON(data); //Parse the json
                $("#status").text("");
                
                var indexOfSymbol = symbols.indexOf(d.symbol); //Check if the symbol has already been added
                if(indexOfSymbol == -1){
                    table.append(createRow(d)); //Add new row if it doesnt exist
                }else{
                    updateRow(table, indexOfSymbol, d); //Update row if it exists
                }
            }catch(err){
                // Add the symbol to the table but make it red and put an error message in the status label
                $("#status").text("Error parsing response");
                var dataRow = $(table[0].insertRow(-1));
                dataRow.append($("<td/>").html($("#symbol").val().toUpperCase()).addClass("text-danger"));
                for(var i = 1; i < headers.length - 1; i++){
                    dataRow.append($("<td/>").html(""));
                }
                dataRow.append($("<td/>").html(createTrashCan()));
            }
        });
    });
});

/**
 * Takes in json data and creates a row based on that data
 * and returns it
 * @param {*JSONdata} data 
 * @returns {jQuery Table Row} dataRow
 */
function createRow(data){
    var symbol = d.symbol;
    if(symbols.indexOf(symbol) == -1){
        symbols.push(symbol);        
    }
    var price = d.l;
    var change = d.c;
    var prctChange = (parseFloat(change) / parseFloat(d.op.replace(",","")))*100;
    var low = d.lo;
    var high = d.hi;
    var volume = d.vo;
    var name = d.name;
    var marketCap = d.mc;
    var exchange = d.exchange;

    var dataRow = $("<tr/>");
    dataRow.append($("<td/>").html(symbol).addClass("text-primary"));
    var image = $("<td/>");
    if(imageNames.indexOf(symbol) != -1){
        var img = $("<img/>").attr("src","img/"+symbol+".png").attr("width",50);
        image.html(img);        
    }
    dataRow.append(image);
    dataRow.append($("<td/>").html(price));
    if(change < 0){
        dataRow.append($("<td/>").html(change).addClass("text-danger"));      
        dataRow.append($("<td/>").html(prctChange.toFixed(3) + " %").addClass("text-danger"));
    }else{
        dataRow.append($("<td/>").html(change).addClass("text-success"));   
        dataRow.append($("<td/>").html( "+" + prctChange.toFixed(3) + " %").addClass("text-success"));
    }
    dataRow.append($("<td/>").html(low));
    dataRow.append($("<td/>").html(high));
    dataRow.append($("<td/>").html(volume));
    dataRow.append($("<td/>").html(name));
    dataRow.append($("<td/>").html(marketCap));
    dataRow.append($("<td/>").html(exchange));
    dataRow.append($("<td/>").html(createTrashCan()));
    return dataRow;
}

//Toggles trashcan visibility
$("#deleteData").click(function(event){
    if(hidden){
        $(".delete").show();
        hidden = false;     
    }else{
        $(".delete").hide();
        hidden = true;
    }
});

/**
 * Takes in the table the index of the symbol and the new data and
 * replaces the current data with the new data
 * @param {*jQuery Table} table 
 * @param {*index of symbol} index 
 * @param {*jsonData} stockData 
 */
function updateRow(table, index, stockData){
    row = table.find('tr').eq(index + 1);
    row.replaceWith(createRow(stockData));
}

/**
 * Creates the trashcan GUI element
 */
function createTrashCan(){
    var trashCan = $("<span/>").addClass("material-icons").addClass("delete").text("delete_forever").click(function(event){
        symbols.splice(symbols.indexOf(symbol),1);
        $(this).parents("tr").replaceWith("");
    });
    if(hidden){
        trashCan.hide();
    }
    return trashCan;
}