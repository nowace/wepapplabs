import {Component} from '@angular/core';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', '../stylesheet.css'],
})
export class AppComponent {
}
