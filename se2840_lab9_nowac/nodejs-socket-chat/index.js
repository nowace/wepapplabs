var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);

var sockets = {};

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/index.html");
});
 
io.on("connection", function (socket) {
    console.log("A user connected: " + socket.id);
    sockets[socket.id] = "" + socket.id;

    socket.emit("chat message", new Message("Welcome to the server! Type /commands to see available commands", "text-success"));
    io.emit("chat message", new Message(sockets[socket.id] + " has connected","text-warning"));
    
    socket.on("disconnect", function () {
        console.log("user disconnected: " + socket.id);
        io.emit("chat message", new Message(sockets[socket.id] + " has disconnected","text-warning"));
        delete sockets[socket.id];
    });

    socket.on('chat message', function (msg) {
        io.emit('chat message',  new Message(sockets[socket.id] + ": " + msg.message,msg.color));
    });

    socket.on("command nick", function(msg){
        if(msg.length > 0 && msg[0].length != 0){
            sockets[socket.id] = msg[0];
            socket.emit("chat message", new Message("Your nickname has been set to: " + msg[0],"text-warning"));
        }
    });

    socket.on("command commands", function(msg){
        socket.emit("chat message", new Message("List of commands: /nick <nickname>, /commands, /whois <nickname>, /whisper <Socket ID> [message]", "text-warning"))
    });

    socket.on("command whois", function(msg){
        var socketID = Object.keys(sockets).find(key => sockets[key] === msg[0]);
        socket.emit("chat message", new Message(msg[0] + " is " + socketID, "text-warning"));
    });

    socket.on("command whisper", function(msg){
        var socketID = msg[0];
        msg.shift();
        io.to(socketID).emit("chat message",new Message(sockets[socket.id] + ": " + msg.join(" "),"text-primary"));
    });
});

http.listen(3000, function () {
    console.log("listening on *:3000");
});

class Message{
    constructor(message, color){
        this.message = message;
        this.color = color;
    }
}
