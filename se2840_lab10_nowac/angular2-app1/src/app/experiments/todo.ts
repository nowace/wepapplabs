

export class Todo{
    title: string ='';
    done: boolean = false;
    id: number;

    constructor(id){
        this.id = id;
    }
}