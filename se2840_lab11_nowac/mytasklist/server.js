var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();
app.use(cors());
app.use(bodyParser.json());
//app.use(cors());

var mangojs = require("mongojs");
var db = mangojs("mongodb://nowace:12345@ds235768.mlab.com:35768/mytasklist",['tasks']);

app.get('/',(req,res)=>{
    res.send("{'msg':'Hello, World!'}");
});

app.get('/api/tasks',function(req,res){
    db.tasks.find(function(err,tasks){
        if(err){
            res.send(err);
        }
        res.json(tasks);
    });
});

app.get('/api/task/:id',function(req,res){
    db.tasks.findOne({_id: mangojs.ObjectId(req.params.id)}, function(err, task){
        if(err){
            res.send(err);
        }
        res.json(task);
    });
});

app.post('/api/task', function(req,res,next){
    var task = req.body;
    if(!task.title || !(task.isDone + '')){
        req.status(400);
        res.json({"Error":"Bad Data"});
    }else{
        db.tasks.save(task,function(err,task){
            if(err){
                res.send(err);
            }
            res.json(task);
        });
    }
});

app.delete('/api/task/:id',function(req,res,next){
    db.tasks.remove({_id: mangojs.ObjectId(req.params.id)}, function(err, task){
        if(err){
            res.send(err);
        }
        res.json(task);
    });
});

app.put('/api/task/:id',function(req, res, next){
    var task = req.body;
    var updateTask = {};

    if(task.isDone){
        updateTask.isDone = task.isDone;
    }

    if(task.title){
        updateTask.title = task.title;
    }

    if(!updateTask){
        res.status(400);
        res.json({
            "Error":"Bad Data"
        });

    }else{
        db.tasks.update({_id: mangojs.ObjectId(req.params.id)}, updateTask,function(err, task){
            if(err){
                res.send(err);
            }
            res.json(task);
        });
    }
});

app.listen(3000, ()=>console.log("Example app listening on port 3000!"));
