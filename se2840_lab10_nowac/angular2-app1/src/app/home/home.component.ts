import {Component} from '@angular/core';

@Component({
  selector: 'home',
  styleUrls: ['./home.component.css', '../../stylesheet.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent {
  name: string = "Eric Nowac";
  phone: string = "1234567890";
  email: string = "nowace@msoe.edu";
  message: string = "Hello, World!";
}
