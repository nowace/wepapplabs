/*
 * se2840_lab6_nowac
 * Eric Nowac
 * Created 1/16/2018
 */
package command.Weather;

/**
 * @author nowace
 * @version 2018.01.16
 */
public final class WeatherResponse{
    private final String city;
    private final String country;
    private final double minTemp;
    private final double maxTemp;
    private final double currentTemp;
    private final double time;

    public WeatherResponse(String city, String country, double minTemp, double maxTemp, double currentTemp, double time){
        this.city = city;
        this.country = country;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.currentTemp = currentTemp;
        this.time = time;
    }

    public double getCurrentTemp() {
        return currentTemp;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public double getTime() {
        return time;
    }
}
