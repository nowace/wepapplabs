import { Component } from '@angular/core';
import { Todo } from './todo';

@Component({
  selector: 'experiments',
  styleUrls: ['./experiments.component.css', '../../stylesheet.css'],
  templateUrl: './experiments.component.html'
})
export class ExperimentsComponent {
  currentID: number = 0;
  currentTodo: Todo = new Todo(this.currentID);
  todos: Todo[] = [];

  addTodo() {
    if(/\S/.test(this.currentTodo.title)){
      this.todos.push(this.currentTodo);
      this.currentID++;
      this.currentTodo = new Todo(this.currentID);
    }
  }

  removeTodo(todo) {
    var index = this.todos.indexOf(todo);
    if(index > -1){
      this.todos.splice(index, 1);      
    }
  }
}

