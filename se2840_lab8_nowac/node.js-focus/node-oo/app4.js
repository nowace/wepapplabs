console.log("app4.js running...");

var Rambo = {
    printFirstName: function(){
        console.log("My name is John");
        console.log(this === Rambo);
    }
};

Rambo.printFirstName();

function doSomething(){
    console.log("\n Rambo needs counseling");
    console.log(this===Rambo);
    console.log(this===global);
}

doSomething();

console.log("app4.js finished");
