/*
 * CommandServlet
 * Eric Nowac
 * Created 1/11/2018
 */
package command.Weather;

import command.Command;
import command.JSON;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * @author nowace
 * @version 2018.01.11
 */
public class WeatherCommand implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String lat = request.getParameter("lat");
        String lng = request.getParameter("lon");
        String url = "http://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lng+"&appid=d943174ad3aceae9dfc2eefdb2e0ffd0";
        String inline = JSON.getJSON(url);
        List<WeatherResponse> responses = new LinkedList<>();
        JSONObject object = new JSONObject(inline);
        JSONObject city = object.getJSONObject("city");
        String cityName = city.getString("name");
        String country = city.getString("country");
        JSONArray list = object.getJSONArray("list");
        int currentDt = list.getJSONObject(0).getInt("dt");

        for(int i = 0; i < list.length(); i++){
            JSONObject currentObject = list.getJSONObject(i);
            if(currentObject.getInt("dt") == currentDt){
                JSONObject main = currentObject.getJSONObject("main");
                responses.add(new WeatherResponse(cityName,country,
                        toF(main.getDouble("temp_min")),
                        toF(main.getDouble("temp_max")),
                        toF(main.getDouble("temp")),
                        currentDt));
                currentDt += 86400;
            }
        }

        request.setAttribute("data",responses);
        System.out.println("Data: " + request.getAttribute("data").toString());
        return "Weather.jsp";
    }

    private double toF(double k){
        return (9/5) * (k - 273) + 32;
    }
}
