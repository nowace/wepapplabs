<%@ page import="command.Weather.WeatherResponse" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: nowace
  Date: 1/16/2018
  Time: 2:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Weather</title>
        <link rel="stylesheet" href="../stylesheet.css">
    </head>
    <body>
        <div class="main">
            <%
                List<WeatherResponse> list = (List<WeatherResponse>)request.getAttribute("data");
                String city = list.get(0).getCity();
                String country = list.get(0).getCountry();
                out.println("<h1>" + city + ", " + country +"</h1>");
                out.println("<div class='flex'>");
                for(int i = 0; i < list.size(); i++){
                    String minTemp = "" + list.get(i).getMinTemp();
                    String maxTemp = "" + list.get(i).getMaxTemp();
                    String currentTemp = "" + list.get(i).getCurrentTemp();

                    out.println("<div class='card text-center cmd-info'>");
                    out.println("<h4>" + new Date((long)list.get(i).getTime() * 1000).toString() + "</h4>");
                    out.println("<p>Max Temp: " + maxTemp.substring(0,maxTemp.indexOf(".")+3) + "</p>");
                    out.println("<p>Min Temp: " + minTemp.substring(0,maxTemp.indexOf(".")+3) + "</p>");
                    out.println("<p>Current Temp: " + currentTemp.substring(0,maxTemp.indexOf(".")+3) + "</p>");
                    out.println("</div>");
                }
                out.println("</div>");
            %>
        </div>
    </body>
</html>
