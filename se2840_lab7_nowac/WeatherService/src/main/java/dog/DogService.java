package main.java.dog;

import main.JSON;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/dog")
public class DogService {

    @Path("{amount}")
    @GET
    @Produces("application/json")
    public Response dog(@PathParam("amount")int amount) throws IOException {
        JSONObject responseObject = new JSONObject();
        JSONArray array = new JSONArray();
        for(int i = 0; i < amount; i++){
            String inline = JSON.getJSON("http://dog.ceo/api/breeds/image/random");
            JSONObject object = new JSONObject(inline);
            array.put(object.getString("message"));
        }
        responseObject.put("urls",array);
        return Response.status(200).header("Access-Control-Allow-Origin","*").entity(responseObject.toString(4)).build();
    }
}
