<%@ page import="command.Time.TimeResponse" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.ZonedDateTime" %><%--
  Created by IntelliJ IDEA.
  User: nowace
  Date: 1/16/2018
  Time: 4:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Time</title>
        <link rel="stylesheet" href="../stylesheet.css">
    </head>
    <body>
        <div class="main flex">
            <%
                List<TimeResponse> list = (List<TimeResponse>)request.getAttribute("data");
                for(int i = 0; i < list.size(); i++){
                    out.println("<div class='card text-center cmd-info'>");
                    out.println("<h4>"+ list.get(i).getName() +"</h4>");
                    out.println("<p>"+ list.get(i).getId() +"</p>");
                    ZonedDateTime time = list.get(i).getDateTime();
                    String timeAsString = String.format("%02d:%02d:%02d",time.getHour(),time.getMinute(),time.getSecond());
                    out.println("<p>"+ timeAsString +"</p>");
                    out.println("</div>");
                }
                List<String> errors = (List<String>)request.getAttribute("errors");
                if(errors.size() > 0){
                    String errorMessage = "Error loading: ";
                    for(int i = 0; i < errors.size(); i++){
                        errorMessage += errors.get(i) + ", ";
                    }
                    out.println("<div class='card text-center cmd-info'>");
                    out.println("<p>"+errorMessage+"</p>");
                    out.println("</div>");
                }
            %>
        </div>
    </body>
</html>
