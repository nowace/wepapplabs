package main.java.weather;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/ctofservice")
public class CtoFService {

    @GET
    @Produces("application/xml")
    public String convertCtoF(){
        double c = 36.8;
        double f = ctof(c);
        String result = "@Produces(\'application/xml\') Output: \n\nC to F Converter Output: \n\n"+f;
        return "<ctofservice>"+
                    "<celsius>" + c + "</celsius>"+
                    "<ctofoutput>" + result + "</ctofoutput>"+
                "</ctofservice>";
    }

    @Path("{c}")
    @GET
    @Produces("application/xml")
    public String convertCtoFfromInput(@PathParam("c")double c){
        double f = ctof(c);
        String result = "@Produces(\'application/xml\') Output: \n\nC to F Converter Output: \n\n"+f;
        return "<ctofservice>"+
                "<celsius>" + c + "</celsius>"+
                "<ctofoutput>" + result + "</ctofoutput>"+
                "</ctofservice>";
    }


    private double ctof(double c){
        return ((c * 9)/5) + 32;
    }
}
