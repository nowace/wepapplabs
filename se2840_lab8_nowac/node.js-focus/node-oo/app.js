console.log("app.js running...");


function placeAnOrder(orderNumber){
    console.log("Customer order number: " + orderNumber);

    cookAndDeliverFood(function (){
        console.log("Delivered food order: " + orderNumber);
    });
}

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms){
        end = new Date().getTime();
    }
}

function cookAndDeliverFood(callback){
    wait(5000);
}


placeAnOrder(1);
placeAnOrder(2);
placeAnOrder(3);
placeAnOrder(4);
placeAnOrder(5);
placeAnOrder(6);

console.log("app.js finished");