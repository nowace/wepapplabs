package controller;

import command.Command;
import command.DogCommand;
import command.TimeCommand;
import command.WeatherCommand;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/*
 * CommandServlet
 * Eric Nowac
 * Created 1/10/2018
 */
@WebServlet(name="CmdServlet",displayName="CmdServlet",urlPatterns={"/CmdServlet"},loadOnStartup=1)
public class CmdServlet extends javax.servlet.http.HttpServlet {
    private HashMap<String, Command> commands;
    private String jspdir = "/";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        initCommands();
    }

    private void initCommands() {
        commands = new HashMap<>();
        commands.put("dog",new DogCommand());
        commands.put("weather", new WeatherCommand());
        commands.put("time",new TimeCommand());
    }

    public void service(HttpServletRequest request, HttpServletResponse response){
        try{
            Command cmd = lookupCommand(request.getParameter("cmd"));
            response.setContentType("text/html");
            PrintWriter output = response.getWriter();
            output.print(cmd.execute(request,response));
            output.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Command lookupCommand(String cmd) {
        return commands.get(cmd);
    }
}
