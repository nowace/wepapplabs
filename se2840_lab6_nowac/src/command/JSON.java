package command;/*
 * se2840_lab6_nowac
 * Eric Nowac
 * Created 1/16/2018
 */

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * @author nowace
 * @version 2018.01.16
 */
public final class JSON {
    public static String getJSON(String url) throws IOException {
        URL jsonCall = new URL(url);
        HttpURLConnection conn = (HttpURLConnection)jsonCall.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();
        int responseCode = conn.getResponseCode();
        String inline = "";
        if(responseCode == 200){
            Scanner scan = new Scanner(jsonCall.openStream());
            while (scan.hasNext()){
                inline += scan.nextLine();
            }
            scan.close();
        }
        return (inline == "")?null:inline;
    }
}
