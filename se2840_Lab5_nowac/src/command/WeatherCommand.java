/*
 * CommandServlet
 * Eric Nowac
 * Created 1/11/2018
 */
package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author nowace
 * @version 2018.01.11
 */
public class WeatherCommand implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String lat = request.getParameter("lat");
        String lng = request.getParameter("lon");
        String file = new String(Files.readAllBytes(Paths.get(request.getServletContext().getRealPath("Weather/weather-service.html"))));
        file = file.replace("{lat}",lat);
        file = file.replace("{lon}",lng);
        return file;

    }
}
