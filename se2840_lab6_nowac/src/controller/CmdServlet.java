package controller;

import command.Command;
import command.Dog.DogCommand;
import command.Time.TimeCommand;
import command.Weather.WeatherCommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/*
 * CommandServlet
 * Eric Nowac
 * Created 1/10/2018
 */
@WebServlet(name="CmdServlet",displayName="CmdServlet",urlPatterns={"/CmdServlet"},loadOnStartup=1)
public class CmdServlet extends javax.servlet.http.HttpServlet {
    private HashMap<String, Command> commands;
    private String jspdir = "/";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        initCommands();
    }

    private void initCommands() {
        commands = new HashMap<>();
        commands.put("dog",new DogCommand());
        commands.put("weather", new WeatherCommand());
        commands.put("time",new TimeCommand());
    }

    public void service(HttpServletRequest request, HttpServletResponse response){
        try{
            Command cmd = lookupCommand(request.getParameter("cmd"));
            String page = cmd.execute(request,response);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(jspdir + page);
            dispatcher.forward(request,response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Command lookupCommand(String cmd) {
        return commands.get(cmd);
    }
}
