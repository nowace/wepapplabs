<%@ page import="controller.CmdServlet" %><%--
  Created by IntelliJ IDEA.
  User: nowace
  Date: 1/10/2018
  Time: 10:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>CmdServlet</title>
    <link rel="stylesheet" href="stylesheet.css">
  </head>
  <body>
    <div class="main flex">
      <div class="card text-center cmd-info">
        <div>
          <img src="http://simpleicon.com/wp-content/uploads/clock-time-1.svg" height="200" width="200">
          <p>Time</p>
          <button class="button-flat" onclick="window.location.href='\\CmdServlet?cmd=time'">Go</button>
        </div>
      </div>
      <div class="card text-center cmd-info">
        <div>
          <img src="https://www.weather2umbrella.com/wp-content/themes/w2u/image/svg/weather-icons/d13.svg" height="200" width="200">
          <p>Weather</p>
          <button class="button-flat" onclick="window.location.href='\\CmdServlet?cmd=weather&lat=43.038902&lon=-87.906471' ">Go</button>
        </div>
      </div>
      <div class="card text-center cmd-info">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Dog.svg/1000px-Dog.svg.png" width="200" height="200">
        <p>Dog picture</p>
        <button class="button-flat" onclick="window.location.href='\\CmdServlet?cmd=dog' ">Go</button>
      </div>
    </div>
  </body>
</html>
