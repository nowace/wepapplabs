<%--
  Created by IntelliJ IDEA.
  User: nowace
  Date: 1/23/2018
  Time: 12:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Help</title>
    <link rel="stylesheet" href="stylesheet.css" type="text/css">
</head>
<body>
    <div>
        <div class="navbar">
            <a href="index.jsp">Home</a>
            <a href="Help.jsp">Help</a>
        </div>
    </div>
    <div class="main">
        <div class="card row">
            <h1>Available Commands</h1>
        </div>
        <div class="card row">
            <h4>Time</h4>
            <p>This command gets the current time in various timezones.</p>
            <p>Example: CmdServlet?cmd=time&zones=all gets all of the timezones</p>
            <p>Example: CmdServlet?cmd=time&zones=pst,mst,cst,est gets the US timezones</p>
        </div>
        <div class="card row">
            <h4>Weather</h4>
            <p>This command gets the 5 day forecast for the city closest to the given location</p>
            <p>Example: CmdServlet?cmd=weather&lat=43.038902&lon=-87.906471 will get the weather in Milwaukee</p>
            <p>Example: CmdServlet?cmd=weather&lat=41.881832&lon=-87.623177 will get the weather in Chicago</p>
        </div>
        <div class="card row">
            <h4>Dog</h4>
            <p>This command gets a random picture of a dog</p>
            <p>Example: CmdServlet?cmd=dog</p>
        </div>
    </div>
</body>
</html>
