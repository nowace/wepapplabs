$(document).ready(function(){
    $.getJSON("http://localhost:8080/service/time/CST,EST,AST,MST,PST",function(data){
        var root = $(".flex");
        var times = data.data;
        for(var i = 0; i < times.length; i++){
            var time = times[i]; 
            root.append(createEntry(time.name,time.id,time.time));
        }
    });
});

function createEntry(name, id, time){
    var div = $("<div/>");
    div.addClass("card");
    div.addClass("text-center");
    div.addClass("cmd-info");
    var timeName = $("<h4/>");
    timeName.text(name);
    var timeID = $("<p/>");
    timeID.text(id);
    var timeTime = $("<p/>") 
    timeTime.text(time);

    div.append(timeName);
    div.append(timeID);
    div.append(timeTime);
    return div;
}