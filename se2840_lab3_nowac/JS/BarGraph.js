/**
 * Author: Eric Nowac <nowace@msoe.edu>
 */


/**
 * A class that represents a Rectangle.
 */
class Rectangle{
    /**
     * Constructs the rectangle with the given parameters and calculates actualWidth and actualSize
     * @param {*Number} x the x position of the rectangle in the graph (Without any spacing applied)
     * @param {*Number} y the y position of the rectangle in the graph (Without any spacing applied)
     * @param {*Number} width the width of the rectangle (no spacing)
     * @param {*Number} height the height of the rectangle (no spacing)
     * @param {*Number} spacing amount to take of width (from both sides)
     * @param {*Point} origin origin of the drawing plan
     * @param {*String} color color of the rectangle
     */
    constructor(x, y, width, height, spacing, origin, color){
        this.position = {
            x: x,
            y: y
        }

        this.size = {
            width: width,
            height: height
        }
        this.color = color;
        this.spacing = spacing;
        this.origin = origin;

        this.actualPosition = {
            x: x + spacing + origin.x,
            y: y + origin.y
        }

        this.actualSize = {
            width: width - (2 * spacing),
            height: height
        }
    }

    /**
     * Draws to the given graphics context (should be a canvas)
     * @param {*Context2D} ctx 
     */
    draw(ctx){
        ctx.beginPath();
        ctx.rect(this.actualPosition.x, this.actualPosition.y, this.actualSize.width, this.actualSize.height);
        ctx.fillStyle = this.color;
        ctx.fill();
        ctx.closePath();
    }
}

/**
 * A class that represents a basic bar graph
 */
class Graph{
    /**
     * Constructs the initial graph
     * @param {*} data data to put into the graph
     * @param {*} id id of the canvas the graph is drawing to
     * @param {*} spacing the spacing between the rectangle and the vertical bar seperating it from the next
     */
    constructor(data,id,spacing){
        this.data = data;
        this.canvasID = id;
        this.spacing = spacing;
        this.canvas = document.getElementById(id);
        this.ctx = canvas.getContext('2d');
        this.rectangles = [];
        //The graph is not at the origin of the canvas. Inner dimensions handle this
        this.innerHeight = canvas.height - 40;
        this.innerWidth = canvas.width - 40;
        this.innerOrigin = {
            x: 20,
            y: 20
        };
        //Spacing of the horizontal indicators
        this.yAxisSpacing = 5;
    }

    /**
     * Creates rectangles from the data with the given color
     * @param {*String} color color of the rectangle
     */
    createRects(color){
        //Calculate the scale factor from the biggest data point
        this.scaleFactor = Math.max(...this.data)/this.innerHeight;        
        this.color = color;
        var width = this.innerWidth;
        var height = this.innerHeight;
        var rectWidth = width/this.data.length;
        //actual spacing prevents the widths from becoming negative
        var actualSpacing = this.spacing;
        if(rectWidth - (2*this.spacing) < 0){
            actualSpacing = 0;
        }

        //Create a rectangle for each data point
        for(var i = 0; i < this.data.length; i++){
            this.rectangles.push(new Rectangle(i * rectWidth,0,rectWidth, this.data[i] / this.scaleFactor,actualSpacing,this.innerOrigin,color));
        }

    }

    /**
     * Draw the axis
     */
    drawAxis(){
        //Calculate the height of the "rectangle" made by the grid
        var height = Math.ceil(this.innerHeight / this.yAxisSpacing);        

        //Draw the x and y axis
        this.ctx.save();
        this.ctx.lineWidth = 0.1;
        this.ctx.translate(0,this.canvas.height);
        this.ctx.scale(1,-1); //Scaling occurs to move the origin of drawing from top left to bottom left
        this.ctx.beginPath();
        this.ctx.moveTo(this.innerOrigin.x,this.canvas.height);
        this.ctx.lineTo(this.innerOrigin.x,this.innerOrigin.y);
        this.ctx.moveTo(this.innerOrigin.x,this.innerOrigin);
        this.ctx.lineTo(this.canvas.width,this.innerOrigin.y);
        this.ctx.stroke();

        //Draw the Vertical indicators
        for(var i = 0; i < this.rectangles.length; i++){
            var rect = this.rectangles[i];
            this.ctx.beginPath();
            this.ctx.moveTo(rect.actualPosition.x + rect.size.width - rect.spacing,rect.actualPosition.y);
            this.ctx.lineTo(rect.actualPosition.x + rect.size.width - rect.spacing,this.canvas.height);
            this.ctx.stroke();
        }

        //Draw the Horizontal indicators
        for(var i = 1; i <= this.yAxisSpacing; i++){
            this.ctx.beginPath();
            this.ctx.moveTo(this.innerOrigin.x,this.innerOrigin.y + (height * i));
            this.ctx.lineTo(this.canvas.width,this.innerOrigin.y + (height * i));
            this.ctx.stroke();
        }
        this.ctx.restore();

        //Draw numbers
        this.ctx.font = "10px Arial";
        for(var i = 0; i < this.rectangles.length; i++){
            var rect = this.rectangles[i];
            this.ctx.fillText(i,rect.actualPosition.x + (rect.actualSize.width/2), this.canvas.height - rect.actualPosition.y + this.innerOrigin.y);
        }

        for(var i = 1; i <= this.yAxisSpacing; i++){
            this.ctx.fillText(((height * i) * this.scaleFactor).toFixed(2), 0, this.canvas.height - (height * i) - this.innerOrigin.y)
        }
    }

    /**
     * Clears the canvas
     */
    clear(){
        this.ctx.clearRect(0,0,this.canvas.width,this.canvas.height);
    }

    /**
     * Draws the rectangles to the canvas
     */
    drawRects(){
        this.ctx.save();
        this.ctx.translate(0,this.canvas.height);
        this.ctx.scale(1,-1);
        for(var i = 0; i < this.rectangles.length; i++){
            this.rectangles[i].draw(this.ctx);
        } 
        this.ctx.restore();
    }

    /**
     * Recalc and redraw the entire graph. Should only be done if the data changed
     * @param {*String} color color of the rectangles
     */
    redraw(color){
        this.clear();
        this.rectangles = [];
        this.scaleFactor = Math.max(...this.data)/this.canvas.height;        
        this.createRects(color);
        this.drawAxis();        
        this.drawRects();
    }

    /**
     * Returns the rectangle at the given client x and client y mouse location
     * @param {*Client x} x 
     * @param {*Client y} y 
     */
    checkRectIntersect(x,y){
        x = x - this.canvas.getBoundingClientRect().left;
        y = y - this.canvas.getBoundingClientRect().top;

        var trueY = this.canvas.height - y;
        var trueX = x;

        for(var i = 0; i < this.rectangles.length; i++){
            var rect = this.rectangles[i];
            if(trueX >= rect.actualPosition.x && 
                trueY >= rect.actualPosition.y && 
                trueX <= rect.actualPosition.x + rect.actualSize.width && 
                trueY <= rect.actualPosition.y + rect.actualSize.height){
                return rect;
            }
        }
        return null;
    }

    /**
     * Resets the colors of the rectangles to what was set at the last createRects call
     */
    resetColor(){
        for(var i = 0; i < this.rectangles.length; i++){
            var rect = this.rectangles[i];
            rect.color = this.color;
        }
    }
}
