$(document).ready(function(){
    $.getJSON("http://localhost:8080/service/forecast/43.038902/-87.906471/F", function (data) {
        console.log(data);
        var root = $(".flex");
        var city = data.City;
        var country = data.Country;
        var unity = data.Unit;
        var temps = data.data;
        var header = $("#header");
        header.text(city + ", " + country);
        for(var i = 0; i < temps.length; i++){
            var temp = temps[i];
            console.log(temp.dt);
            var div = createEntry(temp.temp_min,temp.temp_max,temp.temp,temp.dt);
            root.append(div);
        }
    });
});


function createEntry(tempMin, tempMax, temp, dt){
    var div = $("<div/>");
    div.addClass("card");
    div.addClass("text-center");
    div.addClass("cmd-info");
    var header = $("<h4/>");
    header.text(dt);
    var maxTemp = $("<p/>");
    maxTemp.text("Max Temp: "+tempMax.toFixed(2));
    var minTemp = $("<p/>");
    minTemp.text("Min Temp: "+tempMin.toFixed(2));
    var ttemp = $("<p/>");
    ttemp.text("Current Temp: "+temp.toFixed(2));
    div.append(header);
    div.append(maxTemp);
    div.append(minTemp);
    div.append(ttemp);
    return div;
}