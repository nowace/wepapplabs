
/**
 * Author: Eric Nowac <nowace@msoe.edu>
 */


var div = document.createElement("div");

/**
 * Flips a given number of coins using a random number generator
 * and returns the amount of heads that occured
 * @param {int} numCoins number of coins to flip
 * @returns {int} number of heads that occured
 */
function doSingleFlip(numCoins){
    heads = 0;
    for(i = 0; i < numCoins; i++){
        heads+= Math.round(Math.random());
    }
    return heads;
}

/**
 * Flips a given amount of coins a given amount of times and records
 * the frequency of the heads
 * @param {int[]} frequency array of frequencies
 * @param {int} numCoins number of coins
 * @param {int} numReps number of reps
 */
function flipCoins(frequency, numCoins, numReps){
    for(rep = 0; rep < numReps; rep++){
        heads = doSingleFlip(numCoins);
        frequency[heads]++;
    }
}

/**
 * Prints the histogram of he heads frequency
 * @param {int[]} frequency 
 * @param {int} numCoins 
 * @param {int} numReps 
 */
function printHistogram(frequency, numCoins, numReps){
    div.innerHTML += "Number of times each head count occurred:<br>";

    for(heads = 0; heads <= numCoins; heads++){
        div.innerHTML += " " + heads + "  " + frequency[heads] + "  ";
        
        fractionOfReps = frequency[heads]/numReps;
        numOfAsterisks = Math.round(fractionOfReps * 100);

        for(i = 0; i < numOfAsterisks; i++){
            div.innerHTML += "*";
        }
        div.innerHTML += "<br>";
    }    
}


/**
 * Runs the program and updates the chart and the text version
 * @param {int} numCoins Number of coins
 * @param {int} numReps Number of repetitions
 * @param {Chart} chart Chart to update
 */
function run(numCoins, numReps, chart){
    div.innerHTML = ""; //reset the text version
    frequency = [];

    //Have to fill frequency with 0's so it doesn't throw errors later
    for(i = 0; i < numCoins + 1; i++){
        frequency.push(0);
    }

    var start = (new Date()).getTime(); //Starting time of op

    flipCoins(frequency, numCoins, numReps); //Flip the coins
    printHistogram(frequency, numCoins, numReps); //Print the text version

    var end = (new Date()).getTime(); //Ending time of op

    div.innerHTML += "Coin flipper time: " + (end-start) + "ms<br><br>"; //Print op time to text version

    document.getElementById("timing").innerHTML = (end-start) + "ms"


    //Constructing labels based on frequency length
    labels = [];
    for(i = 0 ; i < frequency.length; i++){
        labels.push("" + i);
    }

    //Constructing chart data based on labels and frequency
    var chartData = {
        labels: labels,
        datasets:[{
            label: "Frequency",
            backgroundColor: "#F44336",
            borderColor: "#FFCDD2",
            data: frequency
        }]
    }

    //Update the chart
    chart.config.data = chartData;
    chart.update();
}

NUM_OF_COINS = 5;
NUM_OF_REPS = 1000;
var ctx = document.getElementById("graph").getContext('2d');

//Initialize a bar graph with default options
var myBar = new Chart(ctx, {
    type: 'bar',
    options: Chart.defaults.bar
});

run(NUM_OF_COINS, NUM_OF_REPS, myBar);

document.getElementById("generatedDiv").appendChild(div);