<%--
  Created by IntelliJ IDEA.
  User: nowace
  Date: 1/16/2018
  Time: 3:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Dog</title>
        <link rel="stylesheet" href="../stylesheet.css">
    </head>
    <body>
        <div class="main">
            <div class="card text-center row">
                <%
                    String src = (String)request.getAttribute("src");
                    out.println("<img class='dog-img' src="+src+">");
                %>
            </div>
        </div>
    </body>
</html>
