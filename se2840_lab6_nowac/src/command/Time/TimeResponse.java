/*
 * se2840_lab6_nowac
 * Eric Nowac
 * Created 1/16/2018
 */
package command.Time;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author nowace
 * @version 2018.01.16
 */
public class TimeResponse {
    private final String name;
    private final ZoneId id;
    private final ZonedDateTime dateTime;

    public TimeResponse(String name, ZoneId id, Instant instant){
        this.name = name;
        this.id = id;
        this.dateTime = ZonedDateTime.ofInstant(instant,id);
    }

    public String getName() {
        return name;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public ZoneId getId() {
        return id;
    }
}
