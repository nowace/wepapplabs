import {Component} from '@angular/core';

@Component({
  selector: 'about',
  styleUrls: ['./about.component.css', '../../stylesheet.css'],
  templateUrl: './about.component.html'
})
export class AboutComponent {
}
