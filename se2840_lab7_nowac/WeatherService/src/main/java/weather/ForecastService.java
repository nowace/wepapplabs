package main.java.weather;

import main.JSON;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;

@Path("/forecast")
public class ForecastService {


    @Path("{lat}/{long}/{unit}")
    @GET
    @Produces("application/json")
    public Response forecast(@DefaultValue("43.038902") @PathParam("lat") double lat,@DefaultValue("-87.906471") @PathParam("long") double lng,@DefaultValue("F") @PathParam("unit")String unit) throws IOException {
        JSONObject returnObject = new JSONObject();
        System.out.println("Lat: " + lat + ", Long: " + lng + ", Unit: " + unit);
        String url = "http://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lng+"&appid=d943174ad3aceae9dfc2eefdb2e0ffd0";
        String inline = JSON.getJSON(url);
        JSONObject object = new JSONObject(inline);
        JSONObject city = object.getJSONObject("city");
        String cityName = city.getString("name");
        String country = city.getString("country");
        JSONArray list = object.getJSONArray("list");
        int currentDt = list.getJSONObject(0).getInt("dt");

        returnObject.put("City",cityName);
        returnObject.put("Country",country);
        returnObject.put("Unit",unit.toUpperCase());
        JSONArray array = new JSONArray();

        for(int i = 0; i < list.length(); i++){
            JSONObject currentObject = list.getJSONObject(i);
            if(currentObject.getInt("dt") == currentDt){
                JSONObject main = currentObject.getJSONObject("main");
                JSONObject weatherResponse = new JSONObject();
                weatherResponse.put("dt", currentObject.getString("dt_txt"));
                if(unit.toUpperCase().equals("C")){
                    weatherResponse.put("temp_min",toC(main.getDouble("temp_min")));
                    weatherResponse.put("temp_max",toC(main.getDouble("temp_max")));
                    weatherResponse.put("temp",toC(main.getDouble("temp")));
                }else if(unit.toUpperCase().equals("F")){
                    weatherResponse.put("temp_min",toF(main.getDouble("temp_min")));
                    weatherResponse.put("temp_max",toF(main.getDouble("temp_max")));
                    weatherResponse.put("temp",toF(main.getDouble("temp")));
                }else{
                    weatherResponse.put("temp_min",(main.getDouble("temp_min")));
                    weatherResponse.put("temp_max",(main.getDouble("temp_max")));
                    weatherResponse.put("temp",(main.getDouble("temp")));
                }
                array.put(weatherResponse);
                currentDt += 86400;
            }
        }
        returnObject.put("data",array);
        return Response.status(200).header("Access-Control-Allow-Origin","*").entity(returnObject.toString(4)).build();
    }


    private double toF(double k){
        return (9/5) * (k - 273) + 32;
    }

    private double toC(double k){
        return k - 273;
    }
}
