package main.java.time;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;

@Path("/time")
public class TimeService {

    @Path("{zones}")
    @GET
    @Produces("application/json")
    public Response time(@PathParam("zones")String timeZones){
        JSONObject responseObject = new JSONObject();
        String[] zones = timeZones.split(",");
        Instant instant = Instant.now();
        JSONArray array = new JSONArray();
        if(timeZones.equals("ALL")){
            ZoneId.getAvailableZoneIds().stream().filter(e->e.contains("America")).forEach(e->{
                //responses.add(new TimeResponse(e,ZoneId.of(e),instant));
                ZonedDateTime time = ZonedDateTime.ofInstant(instant,ZoneId.of(e));
                String timeString = String.format("%02d:%02d:%02d",time.getHour(),time.getMinute(),time.getSecond());
                array.put(new JSONObject().put("time",timeString).put("name",e).put("id",ZoneId.of(e)));
            });
        }else{
            for (String zone : zones) {
                try {
                    ZoneId id = ZoneId.of(ZoneId.SHORT_IDS.get(zone));
                    ZonedDateTime time = ZonedDateTime.ofInstant(instant, id);
                    String timeString = String.format("%02d:%02d:%02d", time.getHour(), time.getMinute(), time.getSecond());
                    array.put(new JSONObject().put("time", timeString).put("name", zone).put("id", id));
                } catch (Exception e) {
                    array.put(new JSONObject().put("Error", zone));
                }
            }
        }
        responseObject.put("data",array);
        return Response.status(200).header("Access-Control-Allow-Origin","*").entity(responseObject.toString(4)).build();
    }
}
