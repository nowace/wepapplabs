console.log("app5.js running...");

function User(){
    this.name = "";
    this.life = 100;
    this.giveLife = function giveLife(targetPlayer){
        targetPlayer.life += 1;
        console.log(this.name + " give 1 life to " + targetPlayer.name);
    }
}

var Erin = new User();
var Rachel = new User();

Erin.name = "Erin";
Rachel.name = "Rachel";

Erin.giveLife(Rachel);
console.log("Erin: " + Erin.life);
console.log("Rachel: " + Rachel.life);

User.prototype.punch = function punch(targetPlayer){
    targetPlayer.life -= 3;
    console.log(this.name + " just punched " + targetPlayer.name);
}

Rachel.punch(Erin);
console.log("Erin: " + Erin.life);
console.log("Rachel: " + Rachel.life);

User.prototype.magic = 60;
console.log("Erin: " + Erin.magic);
console.log("Rachel: " + Rachel.magic);

User.prototype.castFireBall = function castFireBall(targetPlayer){
    targetPlayer.life = 0;
    this.magic -= 30;
    console.log(this.name + " just cast fire ball on " + targetPlayer.name);
}

Erin.castFireBall(Rachel);

console.log("Erin Life: " + Erin.life);
console.log("Rachel Life: " + Rachel.life);
console.log("Erin Magic: " + Erin.magic);
console.log("Rachel Magic: " + Rachel.magic);

console.log("app5.js finished");