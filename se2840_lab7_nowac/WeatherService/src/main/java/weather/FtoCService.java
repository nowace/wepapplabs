package main.java.weather;

import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/ftocservice")
public class FtoCService {

    @GET
    @Produces("application/json")
    public Response convertFtoC(){
        JSONObject jsonObject = new JSONObject();
        double f = 98.24;
        jsonObject.put("F Value", f);
        jsonObject.put("C Value", ftoc(f));

        String result = "@Produces(\'application/json\') Output: \n\nF to C Converter Output: \n\n"+jsonObject;
        return Response.status(200).header("Access-Control-Allow-Origin","*").entity(result).build();
    }

    @Path("{f}")
    @GET
    @Produces("application/json")
    public Response convertFtoCfromInput(@PathParam("f")float f){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("F Value", f);
        jsonObject.put("C Value", ftoc(f));

        String result = "@Produces(\'application/json\') Output: \n\nF to C Converter Output: \n\n"+jsonObject;
        return Response.status(200).header("Access-Control-Allow-Origin","*").entity(result).build();
    }


    private double ftoc(double f){
        return (f-32.0) * (5.0/9.0);
    }
}
