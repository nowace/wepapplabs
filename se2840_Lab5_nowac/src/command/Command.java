/*
 * CommandServlet
 * Eric Nowac
 * Created 1/11/2018
 */
package command;

import java.io.IOException;

/**
 * @author nowace
 * @version 2018.01.11
 */
public interface Command {
    String execute(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws IOException;
}
