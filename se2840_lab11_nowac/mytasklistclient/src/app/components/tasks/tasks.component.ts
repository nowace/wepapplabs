import { Component, OnInit } from '@angular/core';
import { TaskService } from "../../services/task.service";
import { Task } from "../../../../task";

@Component({
  moduleId: module.id,
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
  providers: [TaskService]
})
export class TasksComponent implements OnInit {

  name: string = "Eric's";
  tasks: Task[];
  title: string = "";

  constructor(private taskService:TaskService) { 
    this.taskService.getTasks().subscribe(tasks => {
      this.tasks = tasks;
    });
  }

  addTask(){
    if(/\S/.test(this.title)){
      var newTask = {
        title: this.title,
        isDone: false
      }
  
      this.taskService.addTask(newTask).subscribe(task => {
          this.tasks.push(task);
          this.title = "";
      })
    }
  }

  deleteTask(id){
    this.taskService.deleteTask(id).subscribe(task=>{
      this.tasks = this.tasks.filter(t => t._id != id);
    });
  }

  updateTask(task){
    task.isDone = !task.isDone;
    this.taskService.updateTasks(task).subscribe(tasks=>{
      //Doesn't work without this for some reason
    });
  }


  ngOnInit() {
  }

}
