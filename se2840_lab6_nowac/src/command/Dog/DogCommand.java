/*
 * CommandServlet
 * Eric Nowac
 * Created 1/11/2018
 */
package command.Dog;

import command.Command;
import command.JSON;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * @author nowace
 * @version 2018.01.11
 */
public class DogCommand implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String inline = JSON.getJSON("http://dog.ceo/api/breeds/image/random");
        JSONObject object = new JSONObject(inline);
        request.setAttribute("src",object.getString("message"));
        return "Dog.jsp";
    }
}
