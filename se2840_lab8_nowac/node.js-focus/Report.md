# Questions

## app.js

### What is going on here?
- Node is running the javascript application. 6 orders are being placed.

### How long does it take to process each order?
- 5 seconds

### How long does it take to process all 6 orders
- 30 Seconds

### What is synchronous execution?
- When instructions execute one after another waiting for the previous instruction to finish before starting.

## app2.js

### Now whats happening here?
- all 6 orders are being placed as fast as possible and all 6 orders are being delivered 5 seconds after their origional order was placed

### Why are all of the customer order number being printed right after each other after and initial delay?

- Because that is the order in which the orders came in. Order 1 came in just a bit faster than order 2 so its call back was called before order 2's callback

### How long does it take to process each order?
- 5 seconds

### How long does it take to process all 6 orders?
- 5 seconds

### What is asyncronous execution?
- when bits of code execute before waiting for the previous bit of code to finish.

## app3.js

### What will get printed out?
- toFu

### What's happening here?
- Person is just a refrence (alias) to Rambo so we are really just changing what is in rambo through the person alias

### True or false?
- ==
    - True
- ===
    - False

## app4js

### What is printed?
- My name is John
- true

### Is this === Rambo?
- Yes

### What's happening?
- this is not equal to Rambo in the doSomething function but this does equal global in the doSomething function

## app5.js

### What is happening with the app?
- We are defining a user which has a life count, a name, and a function to increase another players life
- We are then defining 2 User's: Rachel and Erin.
- We are then setting their names.
- Erin then gives Rachel 1 life and we print the life counts of both to see that Rachel did in fact get 1 more life

### What is happening with the app?
- Everything that happened previously but now Rachel punched Erin (which is a little rude)

### What is happening with the app?
- Everything that happened previously but now both Rachel and Erin have magic (which seems like a bad idea with their propensity to violence)

### Added functionality
- Everything that happened previously but now Erin retaliated with magic and killed Rachel (Called it)

## app6.js
