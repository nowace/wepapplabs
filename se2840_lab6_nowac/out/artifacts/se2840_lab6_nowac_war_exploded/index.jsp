<%--
  Created by IntelliJ IDEA.
  User: nowace
  Date: 1/16/2018
  Time: 1:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>CmdServlet</title>
    <link rel="stylesheet" href="stylesheet.css" type="text/css">
  </head>
  <body>
    <div>
      <div class="navbar">
        <a href="index.jsp">Home</a>
        <a href="Help.jsp">Help</a>
      </div>
    </div>
    <div class="main flex">
      <div class="card text-center cmd-info">
        <div>
          <img src="http://simpleicon.com/wp-content/uploads/clock-time-1.svg" height="200" width="200">
          <p>Time</p>
          <button class="button-flat" onclick="window.location.href='\\CmdServlet?cmd=time&zones=pst,mst,cst,est'">Go</button>
        </div>
      </div>
      <div class="card text-center cmd-info">
        <div>
          <img src="https://www.weather2umbrella.com/wp-content/themes/w2u/image/svg/weather-icons/d13.svg" height="200" width="200">
          <p>Weather</p>
          <button class="button-flat" onclick="window.location.href='\\CmdServlet?cmd=weather&lat=43.038902&lon=-87.906471' ">Go</button>
        </div>
      </div>
      <div class="card text-center cmd-info">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Dog.svg/1000px-Dog.svg.png" width="200" height="200">
        <p>Dog picture</p>
        <button class="button-flat" onclick="window.location.href='\\CmdServlet?cmd=dog' ">Go</button>
      </div>
      <div class="card row">
        <span>Enter the Command</span>
        <div>
          <Label for="cmdInput"></Label>
          <input id="cmdInput" value="cmd=time&zones=all">
          <button class="button-flat" onclick="onClick()">Go</button>
        </div>
      </div>
    </div>
    <script>
      function onClick(){
          if(document.getElementById("cmdInput").value != ""){
              window.location.href="\\CmdServlet?"+document.getElementById("cmdInput").value;
          }
      }
    </script>
  </body>
</html>
