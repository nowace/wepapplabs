/*
 * CommandServlet
 * Eric Nowac
 * Created 1/14/2018
 */
package command.Time;

import command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;

/**
 * @author nowace
 * @version 2018.01.14
 */
public class TimeCommand implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String timeZones = request.getParameter("zones").toUpperCase();
        String[] zones = timeZones.split(",");
        Instant instant = Instant.now();
        List<TimeResponse> responses = new LinkedList<>();
        List<String> errors = new LinkedList<>();
        System.out.println(ZoneId.SHORT_IDS);
        if(timeZones.equals("ALL")){
            ZoneId.getAvailableZoneIds().stream().filter(e->e.contains("America")).forEach(e->{
                responses.add(new TimeResponse(e,ZoneId.of(e),instant));
            });
        }else{
            for(int i = 0; i < zones.length; i++){
                try{
                    ZoneId id = ZoneId.of(ZoneId.SHORT_IDS.get(zones[i]));
                    responses.add(new TimeResponse(zones[i],id,instant));
                }catch (Exception e){
                    errors.add(zones[i]);
                }
            }
        }

        request.setAttribute("data",responses);
        request.setAttribute("errors",errors);
        return "Time.jsp";
    }
}
