/**
 * Author: Eric Nowac <nowace@msoe.edu>
 */

var graph = new Graph([],"canvas",12);
graph.yAxisSpacing = 10;
var numCoinsInput = document.getElementById("coinNumField");
var numRepsInput = document.getElementById("repNumField");

/**
 * Flips a given number of coins using a random number generator
 * and returns the amount of heads that occured
 * @param {int} numCoins number of coins to flip
 * @returns {int} number of heads that occured
 */
function doSingleFlip(numCoins){
    heads = 0;
    for(i = 0; i < numCoins; i++){
        heads+= Math.round(Math.random());
    }
    return heads;
}

/**
 * Flips a given amount of coins a given amount of times and records
 * the frequency of the heads
 * @param {int[]} frequency array of frequencies
 * @param {int} numCoins number of coins
 * @param {int} numReps number of reps
 */
function flipCoins(frequency, numCoins, numReps){
    for(rep = 0; rep < numReps; rep++){
        heads = doSingleFlip(numCoins);
        frequency[heads]++;
    }
}


/**
 * Runs the program and updates the chart
 * @param {int} numCoins Number of coins
 * @param {int} numReps Number of repetitions
 */
function run(numCoins, numReps){
    frequency = [];

    //Have to fill frequency with 0's so it doesn't throw errors later
    for(i = 0; i < numCoins + 1; i++){
        frequency.push(0);
    }

    var start = (new Date()).getTime(); //Starting time of op
    flipCoins(frequency, numCoins, numReps); //Flip the coins
    var end = (new Date()).getTime(); //Ending time of op
    graph.data = frequency;
    graph.redraw("#F44336");    
    document.getElementById("timing").innerHTML = "Time to flip coins: " + (end-start) + "ms"
}

/**
 * Selects the rectangle that is being hovered over
 * @param {*MouseEvent} event 
 */
function selectRect(event){
    graph.resetColor();
    var rect = graph.checkRectIntersect(event.clientX,event.clientY);
    if(rect != null){
        rect.color = "#ff2919";
        document.getElementById("xSelect").innerHTML = "Number of Heads: " + Math.round((rect.position.x / rect.size.width));        
        document.getElementById("ySelect").innerHTML = "Frequency: " + Math.round((rect.size.height * graph.scaleFactor));
    }
    graph.clear();
    graph.drawAxis();
    graph.drawRects();
}

/**
 * Re runs the program with new inputs
 */
function refresh(){
    var numCoinsError = checkNumCoins(numCoinsInput.value);
    var numRepsError = checkNumReps(numRepsInput.value);
    document.getElementById("numCoinsError").innerHTML = numCoinsError;
    document.getElementById("numRepsError").innerHTML = numRepsError;
    if(numCoinsError == "" && numRepsError == ""){
        run(Number(numCoinsInput.value), Number(numRepsInput.value));        
    }
}

/**
 * Checks the Num Coins input
 * @param {*Integer} num 
 */
function checkNumCoins(num){
    var reg = new RegExp('^\\d+$');
    if(!reg.test(num)) return "Invalid Number (Has to be Integer)";
    var numAsNum = Number(num);
    if(numAsNum > 10 || numAsNum < 0) return "Invalid range (1-10)"

    return "";
}

/**
 * Checks the Num Reps input
 * @param {*Integer} num 
 */
function checkNumReps(num){
    var reg = new RegExp('^\\d+$');
    if(!reg.test(num)) return "Invalid Number (Has to be Integer)";
    var numAsNum = Number(num);
    if(numAsNum > 100000 || numAsNum < 0) return "Invalid range (1-100,000)"
    return "";
}

//Initial Run
NUM_OF_COINS = 5;
NUM_OF_REPS = 100000;
run(NUM_OF_COINS, NUM_OF_REPS);